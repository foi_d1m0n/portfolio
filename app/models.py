__author__ = 'dmitry'

from flask.ext.login import UserMixin
from . import db
from . import login_manager
from werkzeug.security import generate_password_hash, check_password_hash


class User(UserMixin, db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(64), unique=True, index=True)
    username = db.Column(db.String(64), unique=True, index=True)
    password_hash = db.Column(db.String(128))

    def __init__(self, email, username, password):
        self.username = username
        self.email = email
        self.password = password


    @property
    def password(self):
        raise AttributeError('password is not a readable attribute')

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)


    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


def create_admin_if_needed():
    admin = User.query.filter_by(username='admin').first()
    if admin is None:
        admin = User('dasd@dsa.com', 'admin', '123456')
        db.session.add(admin)
        db.session.commit()