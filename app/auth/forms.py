__author__ = 'dmitry'

from flask.ext.wtf import Form
from wtforms import StringField, PasswordField, BooleanField, SubmitField
from wtforms.validators import DataRequired, Length, Email

class LoginForm(Form):
    username = StringField('Username', [DataRequired(), Length(1, 64)])
    password = PasswordField('Password', [DataRequired()])
    remember_me = BooleanField('Keep me logged in')
    submit = SubmitField('Log In')


