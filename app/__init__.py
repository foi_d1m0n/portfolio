__author__ = 'dmitry'

from flask import Flask
from flask.ext.bootstrap import Bootstrap
from config import config
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.login import LoginManager

bootstrap = Bootstrap()
db = SQLAlchemy()


login_manager = LoginManager()
login_manager.session_protection = 'strong'
login_manager.login_view = 'auth.login'

from models import create_admin_if_needed

def create_app(config_name):
    app = Flask(__name__)
    app.config.from_object(config[config_name])
    config[config_name].init_app(app)

    bootstrap.init_app(app)
    # mail.init_app(app)
    # moment.init_app(app)
    db.init_app(app)
    with app.app_context():
        db.create_all()
        create_admin_if_needed()


    login_manager.init_app(app)

    # attach routes and custom error pages here
    from .auth import auth as auth_blueprint
    app.register_blueprint(auth_blueprint)

    from .projects import projects as projects_blueprint
    app.register_blueprint(projects_blueprint)

    return app